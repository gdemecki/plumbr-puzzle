package slow;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import slow.internal.Source;

public class Worker implements Runnable {

    private final int lineIndex;
    private final CountDownLatch latch;

    public static final List<BigInteger> NUMBERS = new LinkedList<>();

    private static final Object LOCK = new Object();

    // cache is shared between workers
    private static final Cache CACHE = new Cache();

    public Worker(int lineIndex, CountDownLatch latch) {
        this.lineIndex = lineIndex;
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            long start = System.currentTimeMillis();

            synchronized (NUMBERS) {
                int numberIndex = Source.getValueAtLine(lineIndex);
                NUMBERS.add(calculateSecret(BigInteger.valueOf(numberIndex)));
            }
            latch.countDown();
            System.out.printf("Secret calculated in %d ms. Remaining secrets to calculate: %d%n",
                    System.currentTimeMillis() - start, latch.getCount());

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    // TODO [gdemecki]: recursive function! Refactor this!
    public BigInteger calculateSecret(BigInteger index) {
        if (index.compareTo(BigInteger.valueOf(1)) <= 0) {
            return BigInteger.ZERO;
        }
        if (index.equals(BigInteger.valueOf(2))) {
            return BigInteger.ONE;
        }

        synchronized (LOCK) {
            BigInteger value = (BigInteger) CACHE.get(index);
            if (value != null) {
                return value;
            } else {
                value = calculateSecret(index.subtract(BigInteger.ONE)).add(
                        calculateSecret(index.subtract(BigInteger.valueOf(2))));
                CACHE.put(index, value);
                return value;
            }
        }
    }

}
