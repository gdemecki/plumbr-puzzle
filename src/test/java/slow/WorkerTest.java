package slow;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Before;
import org.junit.Test;

public class WorkerTest {

    private static final int WORKLOAD = 100;

    private Worker worker;

    @Before
    public void setUp() {

        final CountDownLatch latch = new CountDownLatch(WORKLOAD);

        int i = 13; // varies from 0 to 100
        // this.worker = new Worker(i, latch);

        this.worker = new Worker(0, null);
    }

    @Test
    public void calculateSecret() {

        BigInteger secret;

        secret = worker.calculateSecret(BigInteger.valueOf(1));
        assertTrue(secret.longValue() == 0);

        secret = worker.calculateSecret(BigInteger.valueOf(2));
        assertTrue(secret.longValue() == 1);

        secret = worker.calculateSecret(BigInteger.valueOf(3));
        assertTrue(secret.longValue() == 1);

        secret = worker.calculateSecret(BigInteger.valueOf(4));
        assertTrue(secret.longValue() == 2);

        secret = worker.calculateSecret(BigInteger.valueOf(5));
        assertTrue(secret.longValue() == 3);

        secret = worker.calculateSecret(BigInteger.valueOf(6));
        assertTrue(secret.longValue() == 5);

        secret = worker.calculateSecret(BigInteger.valueOf(7));
        assertTrue(secret.longValue() == 8);

        secret = worker.calculateSecret(BigInteger.valueOf(8));
        assertTrue(secret.longValue() == 13);

        secret = worker.calculateSecret(BigInteger.valueOf(9));
        assertTrue(secret.longValue() == 21);

        secret = worker.calculateSecret(BigInteger.valueOf(10));
        assertTrue(secret.longValue() == 34);

        secret = worker.calculateSecret(BigInteger.valueOf(11));
        assertTrue(secret.longValue() == 55);

        secret = worker.calculateSecret(BigInteger.valueOf(12));
        assertTrue(secret.longValue() == 89);

        secret = worker.calculateSecret(BigInteger.valueOf(13));
        assertTrue(secret.longValue() == 144);

        secret = worker.calculateSecret(BigInteger.valueOf(21));
        assertEquals(6765, secret.longValue());

        secret = worker.calculateSecret(BigInteger.valueOf(25));
        assertEquals(46368, secret.longValue());

        secret = worker.calculateSecret(BigInteger.valueOf(30));
        assertEquals(514229, secret.longValue());

        secret = worker.calculateSecret(BigInteger.valueOf(37));
        assertEquals(14930352, secret.longValue());
    }

}
